+++
title = "Example Tutorial"
date = 2021-06-13
updated = 2021-06-16

[extra]
author = "Julia Scheaffer"

[taxonomies]
tags = ["test"]
+++
This is an example tutorial

## Subsection

```c
int main(){
    printf("we got some code too.\n");
    return 0;
}
```
